;;; The exercise 5.6 is the first non so trivial one

;;; void -> number
(defun THROW-DIE ()
  "Returns a random number between 1 and 6 (inclusive)"
  (+ (RANDOM 6) 1))

;;; void -> dice
(defun THROW-DICE ()
  "Returns a list of two random dies"
  (list (THROW-DIE) (THROW-DIE)))

;;; dice -> bool
(defun SNAKE-EYES-P (dice)
  "Two ones is called 'snake eyes',
   check if the dice is one"
  (EQUAL dice '(1 1)))

;;; dice -> bool
(defun BOXCARS-P (dice)
  "Two sixes is called 'boxcars',
   check if the dice is one"
  (EQUAL dice '(6 6)))

;;; dice -> bool
(defun INSTANT-WIN-P (dice)
  "Check if the first throw is an instant win (7 or 11)"
  (LET ((SUM-DICE (APPLY #'+ dice)))
    (OR (EQUAL SUM-DICE 7)
        (EQUAL SUM-DICE 11))))

;;; dice -> bool
(defun INSTANT-LOSS-P (dice)
  "Check if the first throw is an instant loss (2 3 or 12)"
  (LET ((SUM-DICE (APPLY #'+ dice)))
    (OR (EQUAL SUM-DICE 2)
        (EQUAL SUM-DICE 3)
        (EQUAL SUM-DICE 12))))

(defun SAY-THROW (dice)
  "Returns either the sum of the two dice or BOXCARS/SNAKE-EYES"
  (COND ((BOXCARS-P dice) 'BOXCARS)
        ((SNAKE-EYES-P dice) 'SNAKE-EYES)
        (T (APPLY #'+ dice))))

(defun craps ()
  ""
  (LET ((roll (THROW-DICE)))
    (LIST
     'THROW
     (FIRST ROLL)
     'AND
     (SECOND ROLL)
     '--
     (SAY-THROW roll)
     '--
     (COND ((INSTANT-WIN-P roll)  'YOU-WIN)
           ((INSTANT-LOSS-P roll) 'YOU-LOSE)
           (T 'KEEP-PLAYING)))))
